import 'dart:ui' as ui;
import 'package:custom_paint_exersice/page_controller2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class MyPainter2 extends CustomPainter {

  MyPainter2({
    required this.index,
    required this.minValue,
    required this.maxValue,
    this.textRightSpace = 20,
    this.leftMargin = 30,
    this.chartHeightFactor = 1,
    this.divisible = 10,
    this.chartSpaceHorizontal = 20,
    this.markerRadius = 5

});
  /// index of the marker
  final int index;

  /// Min and max values of the chart
  final double minValue;
  final double maxValue;

  /// Width of the left Edge to chart
  final double leftMargin;

  /// space between number on Oy and the chart
  final double textRightSpace;

  /// The desired height of chart compare to the original size
  final double chartHeightFactor;

  /// Ox Space
  final double chartSpaceHorizontal;

  /// determine which values will be shown on the Oy axis (eg. -5, 0, 5 or -10, 0, 10)
  final int divisible;

  /// marker circle radius
  final double markerRadius;

  /// Divide line painter setup
  final backgroundLinePainter = Paint()
    ..color = Colors.grey
    ..strokeWidth = 2;

  /// Chart painter setup
  final pathPainter = Paint()
    ..color = Colors.blue
    ..strokeWidth = 2
    ..style = PaintingStyle.stroke;

  /// Marker setup
  final markerPainter = Paint()
    ..color = Colors.blue;
  final markerLinePainter = Paint()
    ..color = Colors.blue
    ..strokeWidth = 1
    ..style = PaintingStyle.stroke;


  /// value indicator by the Oy Axis
  final textPainter = TextPainter(textDirection: ui.TextDirection.ltr);

  @override
  void paint(Canvas canvas, Size size) {
    final PageController2 _controller = Get.find();

    int pointValue = _controller.data[index].temp;

    /// converter
    final double unitToPixel = size.height * chartHeightFactor / (maxValue - minValue);

    Offset markerPoint = Offset(leftMargin + chartSpaceHorizontal * index, size.height - ( pointValue - minValue)*unitToPixel);

    // /// center point
    // Offset center = Offset(size.width / 2, size.height / 2);


    // draw the divisible line and number
    int j = 0;
    for (int i = minValue.toInt(); i < maxValue.toInt() + 1; i++) {
      // draw line to indicate which value is divisible by an number defined above
      if (i % divisible == 0) {
        canvas.drawLine(
            Offset(leftMargin,
                (1 - chartHeightFactor) / 2 * size.height + unitToPixel * divisible * j),

            Offset(size.width,
                (1 - chartHeightFactor) / 2 * size.height + unitToPixel * divisible * j),
            backgroundLinePainter
        );

        // Draw the text
        textPainter.text = new TextSpan(
            text: "${-i}", style: TextStyle(fontSize: 15, color: Colors.black));

        textPainter.layout();
        textPainter.paint(canvas, Offset(leftMargin - (textPainter.width / 2) - textRightSpace,
                (1 - chartHeightFactor) / 2 * size.height +
                    unitToPixel * divisible * j -
                    (textPainter.height / 1.5)));

        // ignore the other values
      } else {
        continue;
      }
      j++;
    }

    // Draw the chart
    // print(_controller.data);
    var dataPath = Path();

    // position of the first value
    dataPath.moveTo(leftMargin, (1 + chartHeightFactor) / 2 * size.height - (_controller.data[0].temp - minValue) * unitToPixel);

    // compute the path
    for (int currentIndex = 0; currentIndex < _controller.data.length; currentIndex++) {

      // if it's the first value, skip the moveTo function
      // if (currentIndex != 0) {
      //   dataPath.moveTo(numberIndicatorWidth + chartSpaceHorizontal * currentIndex.toDouble(),
      //       (1 + chartHeightFactor) / 2 * size.height - (data[currentIndex] - minValue) * unitToPixel);
      // }


      // if it's the last value, stop drawing
      if (currentIndex != _controller.data.length - 1) {
        // dataPath.cubicTo(
        //     numberIndicatorWidth + chartSpaceHorizontal * (currentIndex + 1).toDouble() , // x2
        //     (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex] - minValue) * unitToPixel , // y1
        //     numberIndicatorWidth + chartSpaceHorizontal * currentIndex.toDouble() , // x1
        //     (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex + 1] - minValue) * unitToPixel , //y2
        //     numberIndicatorWidth + chartSpaceHorizontal * (currentIndex + 1).toDouble(),
        //     (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex + 1] - minValue) * unitToPixel);

        // smooth the line
        dataPath.cubicTo(
          ui.lerpDouble(leftMargin + chartSpaceHorizontal * currentIndex.toDouble() , leftMargin + chartSpaceHorizontal * (currentIndex + 1).toDouble() , 0.5)!, // x2
            (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex].temp - minValue) * unitToPixel , // y1
            ui.lerpDouble(leftMargin + chartSpaceHorizontal * currentIndex.toDouble() , leftMargin + chartSpaceHorizontal * (currentIndex + 1).toDouble() , 0.5)!, // x1
            (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex + 1].temp - minValue) * unitToPixel , //y2
            leftMargin + chartSpaceHorizontal * (currentIndex + 1).toDouble(),
            (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex + 1].temp - minValue) * unitToPixel);

        // dataPath.lineTo(numberIndicatorWidth + chartSpaceHorizontal * (currentIndex + 1).toDouble(),
        //     (1 + chartHeightFactor) / 2 * size.height - (_controller.data[currentIndex + 1] - minValue) * unitToPixel);
      }
    }


    // Draw the path (chart)
    canvas.drawPath(dataPath, pathPainter);

    // Draw the marker
    canvas.drawCircle(markerPoint, markerRadius, markerPainter);
    canvas.drawLine(markerPoint, Offset(markerPoint.dx,size.height), markerLinePainter);

    // Draw the Ox Axis text
    textPainter.text = TextSpan(text: DateFormat('yyyy-MM-dd').format(_controller.data.first.date), style: TextStyle(color: Colors.black));
    textPainter.layout();
    textPainter.paint(canvas, Offset(leftMargin, size.height +10));


  }

  @override
  bool shouldRepaint(MyPainter2 oldDelegate) => oldDelegate.index != index;

  // /// Draw text
  // void drawText(Canvas canvas,String text, Offset offset, TextStyle style) {
  //   final textPainter = TextPainter(textDirection: ui.TextDirection.ltr);
  //   textPainter.text = TextSpan(text: text, style: style);
  //   textPainter.layout();
  //   textPainter.paint(canvas, offset);
  // }
}

import 'package:custom_paint_exersice/page_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:math' as math;

class MyPainter extends CustomPainter {

  final E1Controller _controller = Get.find();

  /// circle dimensions
  double innerCircleRadius = 75;
  double outerCircleRadius = 100;
  final outerCicleClosePercent = 0.75; // 75% of a full circle, means 4/3*pi

  late var delta = _controller.lastValueChange;

  // Outer circle setup
  final startAngle = 5 * math.pi / 6;
  late final sweepAngle = math.pi / outerCicleClosePercent;
  late var sweepAngleActive = (value - startValue) * anglePerUnit + delta* anglePerUnit * _controller.quickValueAnimationController.value;
  final useCenter = false;

  /// Dash line length
  double shortLine = 15;
  double longLine = 25;

  /// rotating angle to draw dashline (need to multiply by math.pi times)
  late final double rotateAngle = (1 / outerCicleClosePercent) / values;

  /// space between dashline and outer circle
  double lineSpace = 15;
  /// space between dashline and text
  double textSpace = 40;

  /// the gauge values
  double startValue = -20;
  double endValue = 20;
  late double value = _controller.currentValue.value;
  late double values = endValue - startValue;

  late final anglePerUnit = sweepAngle / values;

  /// calculate the percent of current value
  late double valuePercent = (value - startValue) / values.toDouble();

  /// gradient colors list
  List<Color> gradientColors = [
    Color(0xff6AB9D7),
    Color(0xff68BC2A),
  ];

  late final activeArcPainter = Paint()
    // ..color = Colors.transparent
    ..strokeWidth = 10
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke
    ..shader = LinearGradient(colors: gradientColors)
        .createShader(Rect.fromLTWH(0, 0, 100, 100));

  final inactiveOuterCirclePainter = Paint()
    ..color = Colors.black12
    ..strokeWidth = 10
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke;

  final dashlinePaint = Paint()
    ..color = Colors.grey
    ..strokeWidth = 4;

  // Text painter
  final textPainter = TextPainter(textDirection: TextDirection.ltr);
  // final textPainter2 = TextPainter(textDirection: TextDirection.ltr);

  @override
  void paint(Canvas canvas, Size size) {
    // Get center postion
    final Offset centerPoint = Offset(size.width / 2, size.height / 2);

    final outerCircle = Rect.fromCenter(
        center: centerPoint,
        width: outerCircleRadius * 2,
        height: outerCircleRadius * 2);
    
    var path1 = Path();
    var path2 = Path();

    path2.addRect(outerCircle);

    path1.addArc(outerCircle, startAngle, sweepAngle);


    // draw grey inactive arc below the active one
    canvas.drawArc(outerCircle, startAngle, sweepAngle, useCenter,
        inactiveOuterCirclePainter);

    // active arc
    canvas.drawArc(
        outerCircle, startAngle, sweepAngleActive, useCenter, activeArcPainter);

    /// Draw shadow below the inner circle

    // Path shadowPath = Path();
    // shadowPath.addOval(
    //     Rect.fromCircle(center: centerPoint, radius: innerCircleRadius));
    // canvas.drawShadow(shadowPath, Colors.black, 1.0, false);

    // Draw the inner circle
    // canvas.drawCircle(centerPoint, innerCircleRadius, innerCirclePainter);


    // save canvas [1]
    canvas.save();

    // shift everything to make the center point rotate itself, not the vertex
    canvas.translate(100, 100);

    // rotate to align with the screen, not sure why
    canvas.rotate(-2 / 3 * math.pi);

    // a counting variable outside the loop, just for counting the loop, because value can be negative, maybe there is a better way to do this
    int j = 0;

    // loop to draw the dash line around the circle
    for (int i = startValue.toInt(); i < endValue + 1; i++) {
      // consider value below than input value is active, paint with color
      if (i <= value) {
        dashlinePaint.shader = LinearGradient(colors: gradientColors, begin: Alignment.centerLeft, end: Alignment.centerRight)
            .createShader(Rect.fromLTWH(0, 0, 2, 2));
        // else paint with grey
      } else {
        dashlinePaint.color = Colors.grey;
        dashlinePaint.shader = null;
      }

      // mark the primary dash line (the longer one) which's value % 10 == 0
      if (i % 10 == 0) {
        String temp = i.toString();
        textPainter.text = TextSpan(
            text: temp + "\u2103",
            style: TextStyle(fontSize: 20, color: Colors.black));

        // Draw the primary dash line
        path1.moveTo(0, -lineSpace - outerCircleRadius);
        path1.lineTo(0, -lineSpace - outerCircleRadius - longLine);

        canvas.drawLine(
            Offset(0, -lineSpace - outerCircleRadius),
            Offset(0, -lineSpace - outerCircleRadius - longLine),
            dashlinePaint);

        // Save the current canvas [2], prepare for draw text
        canvas.save();

        // Shift the text to the right position
        canvas.translate(0, -outerCircleRadius - longLine - textSpace);

        // Rotate the canvas for vertical text drawing
        canvas.rotate(-j * rotateAngle * math.pi + 2 / 3 * math.pi);

        textPainter.layout();

        // Draw the text
        textPainter.paint(canvas,Offset(-(textPainter.width / 2), -(textPainter.height / 1.5)));

        // restore the rotation to canvas [2]
        canvas.restore();

        // Secondary dash line
      } else if (i % 2 == 0) {
        canvas.drawLine(
            Offset(0, -lineSpace - outerCircleRadius),
            Offset(0, -lineSpace - outerCircleRadius - shortLine),
            dashlinePaint);
      }

      // Keep rotating the canvas
      canvas.rotate(rotateAngle * math.pi);
      j++;
    }

    // restore to the initial canvas rotation
    canvas.restore();

    var path3 = Path.combine(PathOperation.difference, path1, path2);
    canvas.drawPath(path3, activeArcPainter);
  }

  @override
  bool shouldRepaint(MyPainter oldDelegate) =>
    oldDelegate.sweepAngleActive != sweepAngleActive;
}
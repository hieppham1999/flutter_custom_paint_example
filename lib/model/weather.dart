import 'package:intl/intl.dart';

class Weather {

  const Weather({
    required this.date,
    required this.temp
});

  final DateTime date;
  final int temp;


  @override
  String toString() {
    // TODO: implement toString
    var dateString = DateFormat("yyyy-MM-dd").format(DateTime.parse(date.toString()));
    print("$dateString,$temp");
    return super.toString();
  }
}

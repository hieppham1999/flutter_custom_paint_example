import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:custom_paint_exersice/mypainter.dart';
import 'package:custom_paint_exersice/bt2.dart';
import 'package:custom_paint_exersice/page_controller.dart';

class BT1 extends StatelessWidget {
  const BT1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _controller = Get.put(E1Controller());

    return AnimatedBuilder(
      animation: Listenable.merge([
        _controller.initGaugeAnimationController,
        _controller.quickValueAnimationController
      ]),
      builder: (context, _) {
        return Scaffold(
            floatingActionButton: IconButton(
              icon: Icon(Icons.arrow_forward),
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => BT2()));
              },
            ),
            body: SafeArea(
              child: Container(
                width: Get.width,
                height: Get.height,
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Stack(alignment: Alignment.center, children: [
                      Container(
                        // decoration: BoxDecoration(
                        //   gradient: LinearGradient(colors: [    Color(0xff6AB9D7),
                        //     Color(0xff68BC2A),]),
                        //   // backgroundBlendMode: BlendMode.srcOver
                        // ),
                        child: CustomPaint(
                          size: Size(200, 200),
                          painter: MyPainter(),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          print(_controller.currentValue.value);
                          _controller.increaseTemp(2);

                        },
                        child: ClipOval(
                          child: Container(
                            alignment: Alignment.center,
                            width: 150,
                            height: 150,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    colors: [
                                  Color(0xff6AB9D7),
                                  Color(0xff68BC2A),
                                ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter)),
                            child: Text(
                              (_controller.currentValue.value +
                                          _controller.lastValueChange *
                                              _controller
                                                  .quickValueAnimationController
                                                  .value)
                                      .toInt()
                                      .toString() +
                                  "\u2103",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 36),
                            ),
                          ),
                        ),
                      ),
                    ]),
                    TextButton(
                      onPressed: () {
                        _controller.updateCurrentValue(-20);
                      },
                      child: Text(
                        "Reset",
                        style: TextStyle(color: Colors.red),
                      ),
                    )
                  ],
                )),
              ),
            ));
      },
    );
  }
}

import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:custom_paint_exersice/model/weather.dart';
import 'package:get/get.dart';

class PageController2 extends GetxController {
  /// list contains values of point
  List<Weather> data = [];

  double spaceBetweenDays = 20;

  var indexWeather = (0).obs;

  void setupData() {
    Random random = Random();
    int min = -5;
    int max = 18;
    DateTime date = DateTime.now();

    for (int i = 0; i <= 15; i++) {
      // var randomPos = random.nextBool();
      date = date.add(Duration(days: 1));
      var randomInt = min + random.nextInt(max - min);
      data.add(Weather(date: date, temp: randomInt));
    }
  }

  int calculateNearestPoint(Offset offset) {
    int index = (offset.dx / spaceBetweenDays).round();

    if (index < 0) {
      return 0;
    } else if (index > data.length - 1) {
      return data.length - 1;
    }
    return index;
  }

  void updateIndex(int index) {
    indexWeather.value = index;
  }

  @override
  void onInit() {
    setupData();
    // TODO: implement onInit
    super.onInit();
  }
}

import 'package:custom_paint_exersice/mypainter2.dart';
import 'package:custom_paint_exersice/page_controller2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'const.dart';

class BT2 extends StatelessWidget {
  const BT2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _controller = Get.put(PageController2());

    // print(_controller.data.toString());

    final double chartHeight = Get.height * 0.4;
    final double chartWidth = Get.width;

    final double defaultPadding = 8;
    final double chartLeftMargin = 30;

    return Scaffold(
        floatingActionButton: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        body: SafeArea(
          child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              alignment: Alignment.center,
              children: [
                SizedBox(
                  width: constraints.maxWidth,
                  height: constraints.maxHeight,
                ),
                Container(
                  padding: EdgeInsets.all(defaultPadding),
                  color: Colors.black12,
                  height: backgroundHeight,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "nhiêt độ trung bình".toUpperCase() + "(\u2103)",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: Colors.black45),
                          ),
                          TextButton(
                              onPressed: () {},
                              child: Text("Xem chi tiết",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                      color: Colors.black)))
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        // width: 500,
                        // height: 400,
                        child: Obx(
                          () => CustomPaint(
                            size: Size(chartWidth, chartHeight),
                            painter: MyPainter2(
                                index: _controller.indexWeather.value,
                                minValue: -20,
                                maxValue: 20, textRightSpace: 20, leftMargin: chartLeftMargin, chartSpaceHorizontal: _controller.spaceBetweenDays),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  width: chartWidth,
                  height: chartHeight,
                  // width: constraints.maxWidth,
                  top: 250,
                  left: chartLeftMargin + defaultPadding,
                  child: Container(
                    // width: 350,
                    // height: 500,
                    // color: Colors.yellow,

                    child: Builder(builder: (BuildContext context) {
                      return GestureDetector(
                        onTapDown: (detail) {
                          print("its tapped");

                          final box = context.findRenderObject()! as RenderBox;
                          Offset local =
                              box.globalToLocal(detail.globalPosition);

                          print("local dx : " + local.dx.toString());
                          print("local dy : " + local.dy.toString());

                          int index = _controller.calculateNearestPoint(local);
                          // print("return value is " + index.toString());

                          _controller.updateIndex(index);
                          // print(_controller.indexWeather.value);
                        },
                        onHorizontalDragUpdate: (detail) {
                          print("its Dragged");
                          print("global dx : " +
                              detail.globalPosition.dx.toString());
                          print("global dy : " +
                              detail.globalPosition.dy.toString());

                          final box = context.findRenderObject()! as RenderBox;
                          Offset local =
                              box.globalToLocal(detail.globalPosition);

                          print("local dx : " + local.dx.toString());
                          print("local dy : " + local.dy.toString());

                          int index = _controller.calculateNearestPoint(local);

                          _controller.updateIndex(index);
                          // print(_controller.indexWeather);
                        },
                      );
                    }),
                  ),
                ),
              ],
            );
          }),
        ));
  }
}
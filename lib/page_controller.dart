import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class E1Controller extends GetxController with GetTickerProviderStateMixin {

  late AnimationController initGaugeAnimationController;
  late AnimationController quickValueAnimationController;
  late Animation<double> initGaugeAnimation;
  late Animation<double> quickGaugeAnimation;

  // var oldValue = -10;
  var currentValue = (-3.0).obs;
  var lastValueChange = 0.0;


  void setupGaugeAnimation() {
    initGaugeAnimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1000));
    quickValueAnimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    initGaugeAnimation = Tween(begin: 0.0, end: 1.0).animate(initGaugeAnimationController);
    quickGaugeAnimation = Tween(begin: 0.0, end: 1.0).animate(quickValueAnimationController);
    // updateValueAnimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1000));
    // initGaugeAnimation = Tween(begin: 0.0, end: 1.0).animate(initGaugeAnimationController);
    // initGaugeAnimationController.forward();
  }
  void refreshData() {

    lastValueChange = 0;
  }

  void updateCurrentValue(double targetValue) {
    lastValueChange = targetValue - currentValue.value;

    quickValueAnimationController.forward(from: 0.0).then((value) {
      refreshData();
      currentValue.value = targetValue;
    }
    );
  }

  void increaseTemp(double increment) {
    lastValueChange = increment;
    quickValueAnimationController.forward(from: 0.0).then((value) {
      refreshData();
      currentValue.value += increment;
    });
  }

  @override
  void onInit() {
    setupGaugeAnimation();
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void dispose() {
    initGaugeAnimationController.dispose();
    quickValueAnimationController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

}